# NSCLC_seurat
Single cell rna seq data analysis of Non-small cell lung cancer (NSCLC) dissociated tumor cells from 7 donors

data: https://www.10xgenomics.com/datasets/20-k-mixture-of-nsclc-dt-cs-from-7-donors-3-v-3-1-3-1-standard-6-1-0


# R packages 
- Seurat v5.0.1
- tidyverse v2.0.0
- SingleR v2.2.0
- celldex v1.10.0
- BiocFileCache v2.8.0
